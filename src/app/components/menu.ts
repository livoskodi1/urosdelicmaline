import { Component } from '@angular/core';

@Component({
    selector: 'menu-link',
    templateUrl: './menu.html'
}) 
export class Menu {

    links = [
    {
        naziv: 'Home',
        putanja:'/',
        jesamLiAktivan: false
    },
    {
        naziv: 'Maline',
        putanja:'/maline',
        jesamLiAktivan: false
    },
    {
        naziv: 'Dodaj Malinu',
        putanja:'/malina/new',
        jesamLiAktivan: false
    }

    ]

}