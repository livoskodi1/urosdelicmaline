import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
@Injectable()
export class MalineService {
  constructor(private http: Http) {}

  getMalineObservable() {
    return this.http.get("http://localhost:3000/maline").map(response => {
      return response.json();
    });
  }

  getJednaMalinaObservable(id) {
    return this.http.get("http://localhost:3000/maline/" + id).map(response => {
      return response.json();
    });
  }

  addMalinaObservable(malina) {
    return this.http.post("http://localhost:3000/maline", malina).map(response => {
      return response.json();
    });
  }

  updateMalinaObservable(malina) {
    return this.http.put("http://localhost:3000/maline/"+malina.id, malina).map(response => {
      return response.json();
    });
  }
  deleteJednaMalinaObservable(id) {
    return this.http.delete("http://localhost:3000/maline/" + id).map(response => {
      return response.json();
  });
}
}