import { Injectable } from "@angular/core";
import { Malina } from "../interfaces/malina";
import { MalineService } from "./maline.service";
import { jednaMalina } from "../routes/jednaMalina/jednaMalina";

@Injectable()
export class MalineModel {
  maline = [];

  constructor(private service: MalineService) {
    this.refreshujMaline();
  }

  refreshujMaline() {
    this.service.getMalineObservable().subscribe(maline => {
      this.maline = maline;
    });
  }
  obrisiMalinu(id, clbk) {
    this.service.deleteJednaMalinaObservable(id).subscribe(() => {
      this.refreshujMaline();
      clbk();
    });
  }
  dodajMalinu(malina, clbk) {
    this.service.addMalinaObservable(malina).subscribe(malina => {
      this.maline.push(malina);
      clbk();
    });
  }

  getMalinuById(id,clbk) {
    for (var i = 0; i < this.maline.length; i++) {
      if (this.maline[i].id == id) {
        clbk(this.maline[i]);
      }
    }
  }

  initializeMalineWithCallback(clbk){
    if(!this.maline || this.maline.length<1){
      this.service.getMalineObservable().subscribe((maline) =>{
        this.maline = maline;
        clbk();
      })
    }else{
      clbk();
    }
  }
  updateMalina(malina){
    this.service.updateMalinaObservable(malina).subscribe((malina)=>{
      this.refreshujMaline();
    })
  }
}


 

