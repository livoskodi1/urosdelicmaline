
export const  colors = [
    {
      label: "Red",
      key: "red"
    },
    {
      label: "Green",
      key: "green"
    },
    {
      label: "White",
      key: "white"
    },
    {
      label: "Black",
      key: "black"
    },
    {
      label: "Orange",
      key: "orange"
    }
  ];