import { Component } from "@angular/core";
import { MalineModel } from "../services/maline.model";
import { Router } from "@angular/router";
import {colors} from "../configs/colors";

@Component({
  templateUrl: "./new.malina.html"
})
export class NewMalina {
  constructor(private malineModel: MalineModel, private router:Router) {}
 
  malina = {};
  colors = colors;

  dodajMalinu() {
    if (this.malina['weight'] && this.malina['nutritionValue']) {
      this.malineModel.dodajMalinu(this.malina, () => {
        this.router.navigate(["/maline"]);
      });
    } else {
      alert("nije sve definisano!");
    }
  }
}
