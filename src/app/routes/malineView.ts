import { MalineModel } from "./../services/maline.model";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
@Component({
  templateUrl: "./maline.html"
})
export class malineView {
  constructor(public malineModel: MalineModel, private router: Router) {}

  otvoriMalinu(id) {
    this.router.navigate(["/maline", id]);
  }
  editujMalinu(id) {
    this.router.navigate(["/maline", id, 'edit']);
  }

  obrisiMalinu(id) {
    this.router.navigate(["/maline", id, 'delete']);
  }
}
