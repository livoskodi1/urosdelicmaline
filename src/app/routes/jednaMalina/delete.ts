import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MalineModel } from "../../services/maline.model";
@Component({
  templateUrl: "./delete.html"
})
export class Delete implements OnInit {
  id;

  constructor(private route: ActivatedRoute, private malineModel:MalineModel, private router: Router) {}

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      this.id = params.id;
    });
  }

  obrisiMalinu() {
    this.malineModel.obrisiMalinu(this.id,() => {
        this.router.navigate (['/maline']);
    });
  }
}
