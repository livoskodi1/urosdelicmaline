import { Component, OnInit } from "@angular/core";
import { colors } from "../../configs/colors";
import { Router, ActivatedRoute } from "@angular/router";
import { MalineModel } from "../../services/maline.model";
@Component({
  selector: "malina-edit",
  templateUrl: "./edit.html"
})
export class Edit implements OnInit{
  malina = {};
  id;
  nutritionValue;
  weight;
  boja;
  colors = colors;

  constructor(private route:ActivatedRoute, private malinaModel: MalineModel, private router:Router){

  }
  ngOnInit(){
    this.route.parent.params.subscribe((params)=>this.id = params.id);
    console.log(this.id);
   // this.malinaModel.refreshujMaline();
    //this.malina = ;
    
    this.malinaModel.initializeMalineWithCallback(()=>{
      this.setMalinaVars();
    })
    
  }

  setMalinaVars(){
    this.malinaModel.getMalinuById(this.id,({nutritionValue,weight,color})=>{
      this.nutritionValue=nutritionValue;
      this.weight = weight;
      this.boja = color;
    })
  }

  izmeniMalinu(){
    const {id,nutritionValue,weight,boja} = this;
    this.malinaModel.updateMalina({id,nutritionValue,weight,color:boja});
    this.router.navigate(['/maline']);
  }
}
