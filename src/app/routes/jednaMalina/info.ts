import { Component, OnInit } from "@angular/core";
import { colors } from "../../configs/colors";
import { ActivatedRoute, Router } from "@angular/router";
import { MalineModel } from "../../services/maline.model";

@Component({
  templateUrl: "./info.html"
})
export class Info {
  id;
  nutritionValue;
  weight;
  color;

  constructor(public activatedRoute: ActivatedRoute, public malineModel: MalineModel) {}

  // ngOnInit pokrece se posle konstruktora 
  ngOnInit() {
    this.activatedRoute.parent.params.subscribe(({id}) => {
      this.id = id;
    });
    this.malineModel.initializeMalineWithCallback(()=>{
        this.setMalineVars();
    })
  }
  setMalineVars() {
    this.malineModel.getMalinuById(
      this.id,
      ({ nutritionValue, weight, color }) => {
        this.nutritionValue = nutritionValue;
        this.weight = weight;
        this.color = color;
      }
    );
  }

}
