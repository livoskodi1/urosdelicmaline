import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
@Component({
  templateUrl: "./jednaMalina.html"
})
export class jednaMalina {
  id;
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(paramsObject => {
      this.id = paramsObject["id"];
    });
  }
}
