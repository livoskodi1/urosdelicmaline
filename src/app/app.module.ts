import { MalineService } from "./services/maline.service";
import { Info } from "./routes/jednaMalina/info";
import { Menu } from "./components/menu";
import { jednaMalina } from "./routes/jednaMalina/jednaMalina";
import { malineView } from "./routes/malineView";
import { homeView } from "./routes/homeView";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule, Component } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { Edit } from "./routes/jednaMalina/edit";
import { MalineModel } from "./services/maline.model";
import { HttpModule } from "@angular/http";
import { Delete } from "./routes/jednaMalina/delete";
import { NewMalina } from "./routes/new.malina";
import { FormsModule } from "@angular/forms";
import { SearchPipe } from './search.pipe';

const routes: Routes = [
  { path: "", component: homeView },
  { path: "maline", component: malineView },
  { path: "malina/new", component: NewMalina },
  {
    path: "maline/:id",
    component: jednaMalina,
    children: [
      { path: "", redirectTo: "info", pathMatch: "full" },
      { path: "edit", component: Edit },
      { path: "info", component: Info },
      { path: "delete", component: Delete }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    malineView,
    jednaMalina,
    homeView,
    Menu,
    Info,
    Edit,
    Delete,
    NewMalina,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpModule,
    FormsModule
  ],
  providers: [MalineService, MalineModel],
  bootstrap: [AppComponent]
})
export class AppModule {}
