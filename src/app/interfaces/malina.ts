
export interface Malina {
  id: number;
  nutritionValue: number;
  weight: string;
  color?: string;
}
